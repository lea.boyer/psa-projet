# PSA-Projet

# Installation:

The following dependencies are required:
- Armadillo
- Python3
    - numpy
    - pymongo
    - Pillow
    - argparse
    - logging
    - bson
    - scipy

Bindings are provided and do not need to be built.

## Setup the mongodb database

Default credentials are:

```
dbname: psa_projet
user: user1
pwd: pwd1
```

Copy mongodb.conf:

```
cp mongodb.conf /etc/mongodb.conf
systemctl restart mongodb.service
```

Create db and user account:
```
use psa_projet
db.createUser({"user": "user1", "pwd": "pwd1", "roles": ["readWrite"] })
```

## Setup a venv

```
python3 -m venv venv
source venv/bin/activate
pip install numpy pymongo Pillow argparse scipy pyevtk
```

## Building

```
make
```

# Usage:

## Generator

```
./generator.py -c examples/gaussian.json -f
```

## Run

```
./run.py -i gaussian
```

## Post

```
./post.py -i gaussian
```

# Developpment

```
make clean
make dev
```

Regenerates bindings

# Tests:

```
pytest
```
