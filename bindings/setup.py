from setuptools import setup, Extension

module = Extension('_solver',
                    include_dirs = ['armanpy', '../venv/lib/python3.10/site-packages/numpy/core/include/'],
                    libraries = ['m', 'z', 'armadillo'],
                    sources = ['solver.i'],
                    extra_objects = ['../src/release/libsolver.a'],
                    swig_opts = ["-c++", "-Wall", "-I.", "-Iarmanpy"])

setup (name = 'package_solver',
       py_modules = ['solver'],
       version = '1.0',
       description = 'The solver',
       ext_modules = [module])
