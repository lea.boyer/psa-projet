#!/usr/bin/env python3

"""!
@brief Run the solver from the last iteration of a run_id.
"""

from pymongo import DESCENDING
import pymongo.errors
import bson
import numpy as np
import pickle
import argparse
import logging
import time
import signal

import solver
from helper import wave_coll, norm


exit_next = False

def handler(sig, frame):
    """!
    Handle sigint
    """
    global exit_next
    logging.warning("run will stop after next checkpoint")
    exit_next = True


signal.signal(signal.SIGINT, handler)

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description='Runs the solver')
parser.add_argument('-i', metavar='run_id', help='identifier for a specific run')
args = parser.parse_args()
run_id = args.i

# Get the last iteration of the run_id to start with
run = wave_coll.find({"run_id": run_id}).sort('iter', DESCENDING).limit(1)[0]
if not run:
    logging.error("Could not find associated wave function")
    exit(1)

current_iter = run["iter"]

conf_dim = run["dim"]
dx = (conf_dim["max_x"] - conf_dim["min_x"])/conf_dim["nbp_x"]
dy = (conf_dim["max_y"] - conf_dim["min_y"])/conf_dim["nbp_y"]
dt = conf_dim["dt"]
niter = conf_dim["niter"]

conf_solver = run["solver"]
eps = conf_solver["eps"]
save = conf_solver["save"]
name = conf_solver["name"]

v = pickle.loads(run["field"])
wave_r = pickle.loads(run["r"])
wave_i = pickle.loads(run["i"])
wave = wave_r + 1j*wave_i

# Call the c++ solver
sol = solver.Solver(psi=wave, v=v, dx=dx, dy=dy, dt=dt, eps=eps)

if current_iter != 0:
    logging.warning(f"resuming previous run from iter {current_iter}/{niter}")


step = {
    "ftcs": sol.FTCS,
    "btcs": sol.BTCS,
    "ctcs": sol.CTCS,
    "ctcsopti": sol.CTCSopti
}[name]

logging.info(f"Norm: {norm(sol.psi)}")

start_time = time.time()
# while we have not reached the right number of iterations, we continue to generate waves
while current_iter < niter:
    step()
    current_iter += 1
    wave = sol.psi

    # insert one of [save] iteration in the db
    if current_iter % save == 0:
        logging.info(f"saved iter: {current_iter}/{niter}")
        logging.info(f"norm: {norm(wave)}")
        end_time = time.time()
        logging.info(f"time elapsed: {end_time - start_time}s")

        # convert the real part of the processed wave
        bin_wave_r = bson.binary.Binary(pickle.dumps(np.array(np.real(wave), dtype=float, order="F")))
        # convert the imaginary part of the processed wave
        bin_wave_i = bson.binary.Binary(pickle.dumps(np.array(np.imag(wave), dtype=float, order="F")))
        
        # try to add a new iteration
        try:
            wave_data = {
                "run_id": run_id,
                "niter": niter,
                "iter": current_iter,
                "field": run["field"],
                "r": bin_wave_r,
                "i": bin_wave_i,
                "solver": conf_solver,
                "dim": conf_dim
            }

            wave_coll.insert_one(wave_data)
        except pymongo.errors.OperationFailure as e:
            logging.error(f"ERROR: {e}")
            exit(1)

        if exit_next:
            exit(0)
