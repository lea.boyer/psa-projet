#include <armadillo>
#include "solver.h"

int main() {
    int nx = 251;
    int ny = 241;

    arma::mat v = arma::zeros(nx,ny);

    arma::cx_mat psi = arma::zeros<arma::cx_mat>(nx, ny);
    arma::vec x = arma::linspace(-1, 1, nx);
    arma::vec y = arma::linspace(-1, 1, ny);

    const arma::cx_double ival = arma::cx_double(0.0, 1.0);
    double kx = 5.;
    double ky = 1.;
    for (int i=0; i < nx; i++) {
        for (int j=0; j < ny; j++) {
            psi(i, j) = exp(-(x[i]*x[i]+y[j]*y[j]))*exp(ival*(kx*x[i]+ky+y[j]));
        }
    }
    psi = psi / arma::norm(psi);
    Solver s = Solver(psi, v, 0.1, 0.1, 0.005, 0.01);

    for(int i=0; i<10; i++) {
        s.CTCS();
    }
}