#include "solver.h"

#define likely(x)      __builtin_expect(x, 1)
#define unlikely(x)    __builtin_expect(x, 0)

#define NO_SHIFT submat(1, 1, nx, ny)
#define XPDX submat(2, 1, nx+1, ny)
#define XMDX submat(0, 1, nx-1, ny)
#define YPDY submat(1, 2, nx, ny+1)
#define YMDY submat(1, 0, nx, ny-1)

Solver::Solver(arma::cx_mat _psi, arma::mat _v, double _dx, double _dy, double _dt, double _eps=1e-3) :
psi(_psi), v(_v), dx(_dx), dy(_dy), dt(_dt), eps(_eps) {
    nx = psi.n_rows;
    ny = psi.n_cols;
    psi_pad = arma::zeros<arma::cx_mat>(nx+2, ny+2);
    psi_swap = arma::zeros<arma::cx_mat>(nx+2, ny+2);
    psi_orig = arma::zeros<arma::cx_mat>(nx+2, ny+2);

    psi_pad.NO_SHIFT = _psi; /// set value of psi_pad to provided psi

    // precalculated values
    arma::cx_double tmp = i*dt*0.5;
    CTCSopti_a = hbar/(2*m*dx*dx)*tmp;
    CTCSopti_b = hbar/(2*m*dx*dx)*tmp;
    CTCSopti_cst=(-v/hbar - hbar/(m*dx*dx) - hbar/(m*dy*dy))*tmp;
}

/**
 * Updates psi numpy array with values from our padded psi.
 */
void Solver::update_psi() {
    psi = psi_pad.NO_SHIFT;
}

/**
 * Updates psi using the Forward Time Centered Space method.
 */
void Solver::FTCS() {
    // *INDENT-OFF*
    psi_pad.NO_SHIFT = psi_pad.NO_SHIFT + i*dt*(
        (-v/hbar - hbar/(m*dx*dx) - hbar/(m*dy*dy))%psi_pad.NO_SHIFT
        + hbar/(2*m*dx*dx)*(psi_pad.XPDX + psi_pad.XMDX)
        + hbar/(2*m*dy*dy)*(psi_pad.YPDY + psi_pad.YMDY)
    );
    // *INDENT-ON*
    update_psi();
}

/**
 * Updates psi using the Backward Time Centered Space method.
 */
void Solver::BTCS() {
    psi_orig = psi_pad;
    arma::cx_mat* psi_a = &psi_pad;
    arma::cx_mat* psi_b = &psi_swap;
    arma::cx_mat* swp;

    do {
        // *INDENT-OFF*
        psi_b->NO_SHIFT = psi_orig.NO_SHIFT + i*dt*(
            (-v/hbar - hbar/(m*dx*dx) - hbar/(m*dy*dy))%psi_a->NO_SHIFT
            + hbar/(2*m*dx*dx)*(psi_a->XPDX + psi_a->XMDX)
            + hbar/(2*m*dy*dy)*(psi_a->YPDY + psi_a->YMDY)
        );
        // *INDENT-ON*

        // swap psi_a and psi_b
        swp = psi_a;
        psi_a = psi_b;
        psi_b = swp;
        //printf("%f\n", arma::norm(psi_pad.NO_SHIFT - psi_swap.NO_SHIFT));
    } while(arma::norm(psi_pad.NO_SHIFT - psi_swap.NO_SHIFT) > eps);

    update_psi();
}


/**
 * Updates psi using the Centered Time Centered Space method.
 */
void Solver::CTCS() {
    psi_orig = psi_pad;
    arma::cx_mat* psi_a = &psi_pad;
    arma::cx_mat* psi_b = &psi_swap;
    arma::cx_mat* swp;

    do {
        // *INDENT-OFF*
        psi_b->NO_SHIFT = psi_orig.NO_SHIFT + i*dt*0.5*(
            (-v/hbar - hbar/(m*dx*dx) - hbar/(m*dy*dy))%psi_orig.NO_SHIFT
            + hbar/(2*m*dx*dx)*(psi_orig.XPDX + psi_orig.XMDX)
            + hbar/(2*m*dy*dy)*(psi_orig.YPDY + psi_orig.YMDY)
            + (-v/hbar - hbar/(m*dx*dx) - hbar/(m*dy*dy))%psi_a->NO_SHIFT
            + hbar/(2*m*dx*dx)*(psi_a->XPDX + psi_a->XMDX)
            + hbar/(2*m*dy*dy)*(psi_a->YPDY + psi_a->YMDY)
        );
        // *INDENT-ON*   

        // swap psi_a and psi_b
        swp = psi_a;
        psi_a = psi_b;
        psi_b = swp;
        //printf("%f\n", arma::norm(psi_pad.NO_SHIFT - psi_swap.NO_SHIFT));
    } while(arma::norm(psi_pad.NO_SHIFT - psi_swap.NO_SHIFT) > eps);
    update_psi();
}

/**
 * Updates psi using the optimized Centered Time Centered Space method.
 */
void Solver::CTCSopti() {
    psi_orig = psi_pad;
    arma::cx_mat* psi_a = &psi_pad;
    arma::cx_mat* psi_b = &psi_swap;
    arma::cx_mat* swp;

    do {
        // *INDENT-OFF*
        psi_b->NO_SHIFT = psi_orig.NO_SHIFT
            + CTCSopti_cst%(psi_orig.NO_SHIFT + psi_a->NO_SHIFT)
            + CTCSopti_a*(psi_orig.XPDX + psi_orig.XMDX + psi_a->XPDX + psi_a->XMDX)
            + CTCSopti_b*(psi_orig.YPDY + psi_orig.YMDY + psi_a->YPDY + psi_a->YMDY)
        ;
        // *INDENT-ON*

        // swap psi_a and psi_b
        swp = psi_a;
        psi_a = psi_b;
        psi_b = swp;
        //printf("%f\n", arma::norm(psi_pad.NO_SHIFT - psi_swap.NO_SHIFT));
    } while(arma::norm(psi_pad.NO_SHIFT - psi_swap.NO_SHIFT) > eps);
    update_psi();
}

#undef NO_SHIFT
#undef XPDX
#undef XMDX
#undef YPDY
#undef YMDY
