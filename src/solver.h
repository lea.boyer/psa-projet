#ifndef SOLVER_H
#define SOLVER_H

#include <armadillo>

/**
 * Class for a solver, called by run.py to calculate the waves.
 */
class Solver {
public :
    Solver(arma::cx_mat psi, arma::mat v, double dx, double dy, double dt, double eps);
    void FTCS();
    void BTCS();
    void CTCS();
    void CTCSopti();

    /**
     * Reference to python numpy array
     */
    arma::cx_mat psi;

private:
    /**
     * Update psi with FTCS, BTCS or CTCS according to the field and the wave function
     */
    void update_psi();

    arma::mat v;
    double dx;
    double dy;
    double dt;
    double eps;

    int nx;
    int ny;

    /* Constants */
    arma::cx_double i = arma::cx_double(0.0, 1.0);
    double hbar = 1.;
    double m = 1.;

    /**
     * Padded version of psi used for calculations
     */
    arma::cx_mat psi_pad;

    /**
     * Used in BTCS, CTCS for iterating during convergence
     */
    arma::cx_mat psi_swap;

    /**
     * Used in CTCS to retain first value of psi.
     */
    arma::cx_mat psi_orig;

    /* Precalculated variables for CTCS */
    arma::cx_double CTCSopti_a;
    arma::cx_double CTCSopti_b;
    arma::cx_mat CTCSopti_cst;
};

#endif //SOLVER_H
