#!/usr/bin/env python3

"""!
@brief Convert the wave of a run_id into vtk files to process later.
"""

import matplotlib.pyplot as plt
import argparse
import logging
import numpy as np
import pickle
from pyevtk.hl import imageToVTK
from helper import wave_coll, norm

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description='Post processing of the solver result')
parser.add_argument('-i', metavar='run_id', help='identifier for a specific run')
args = parser.parse_args()
run_id = args.i

# gets all the runs from the db with the same run_id
runs = [i for i in wave_coll.find({"run_id": run_id})]
if not runs:
    logging.error("Could not find associated wave function")
    exit(1)

conf_dim = runs[0]["dim"]
max_x = conf_dim["max_x"]
min_x = conf_dim["min_x"]
nbp_x = int(conf_dim["nbp_x"])
max_y = conf_dim["max_y"]
min_y = conf_dim["min_y"]
nbp_y = int(conf_dim["nbp_x"])

 # for each run, convert the wave, proba, phase and field into a vtk file.
for run in runs:
    filename = f"vtk/{run['run_id']}-{run['iter']:09}"
    wave_r = pickle.loads(run["r"])
    wave_i = pickle.loads(run["i"])
    wave = wave_r + 1j*wave_i
    proba = np.abs(wave)**2

    field = pickle.loads(run["field"])
    phase = np.angle(wave)
    imageToVTK(filename, pointData={
        'wave_r': wave_r.reshape((nbp_x, nbp_y, 1), order='C'),
        'wave_i': wave_i.reshape((nbp_x, nbp_y, 1), order='C'),
        'proba': proba.reshape((nbp_x, nbp_y, 1), order='C'),
        'phase': phase.reshape((nbp_x, nbp_y, 1), order='C'),
        'field': field.reshape((nbp_x, nbp_y, 1), order='C')
    })

x = []
y = []
#plot the runs
for run in sorted(runs, key=lambda i: i['run_id']):
    wave_r = pickle.loads(run["r"])
    wave_i = pickle.loads(run["i"])
    wave = wave_r + 1j*wave_i
    x.append(run['iter'])
    y.append(norm(wave))

plt.xlabel('iter')
plt.ylabel('norm')
plt.plot(x, y)
plt.show()
