#!/usr/bin/env python3

"""!
@brief Generates the wave function and the potential by processing a json file with all the arguments needed.
"""
import argparse
import bson
import json
import logging
import numpy as np
import pickle
import pymongo.errors
from helper import wave_coll, gaussian, load_field, ho2d, norm, normalize, hov

logging.basicConfig(level=logging.INFO)

# Recovers the arguments (json file)
parser = argparse.ArgumentParser(description='Generates field')
parser.add_argument('-c', metavar='conf.json', help='config file', default="conf.json")
parser.add_argument('-f', help='overwrite run with same id', action="store_true")
args = parser.parse_args()
logging.info(f"config file: {args.c}")
jsonContent = open(args.c, "r").read()
conf = json.loads(jsonContent)

# Check run_id
run_id = conf["run_id"]
if not run_id:
    logging.error("no run id specified in conf")
    exit(1)

if wave_coll.find_one({"run_id": run_id}):
    if args.f:
        logging.info(f"run with run_id {run_id} already exists, overwriting")
        wave_coll.delete_many({"run_id": run_id})
    else:
        logging.error(f"run with run_id {run_id} already exists, aborting")
        exit(1)

## Check keys
conf_solver = conf["solver"]
name = conf_solver["name"]
save = int(conf_solver["save"])
eps = conf_solver["eps"]

conf_dim = conf["dim"]
max_x = conf_dim["max_x"]
min_x = conf_dim["min_x"]
nbp_x = int(conf_dim["nbp_x"])
max_y = conf_dim["max_y"]
min_y = conf_dim["min_y"]
nbp_y = int(conf_dim["nbp_x"])
dt = conf_dim["dt"]

## t_max to n_iter
tmax = conf_dim.get("tmax")
niter = conf_dim.get("niter")
if tmax:
    conf_dim["niter"] = tmax/dt
elif niter:
    conf_dim["tmax"] = niter*dt
else:
    logging.error("number of iterations not defined (tmax or niter)")
    exit(1)
niter = conf_dim["niter"]
tmax = conf_dim["tmax"]
logging.info(f"niter: {niter} | tmax: {tmax}")

# Compute field from config
field = np.zeros((nbp_x, nbp_y), dtype=float)
for field_conf in conf["field"]:
    if field_conf["method"] == "gaussian":
        field += np.real(gaussian(
            field_conf["x0"], field_conf["y0"], field_conf["w"], 0, 0,
            min_x, max_x, nbp_x,
            min_y, max_y, nbp_y
        ) * field_conf["A"])
    elif field_conf["method"] == "image":
        field += load_field(field_conf["image"], nbp_x, nbp_y)
    elif field_conf["method"] == "hov":
        field += hov(
            min_x, max_x, nbp_x,
            min_y, max_y, nbp_y
        )
    else:
        logging.error("no field definition provided")
        exit(1)

# Compute wave from config
wave = np.zeros((nbp_x, nbp_y), dtype=complex)
for wave_conf in conf["wave"]:
    if wave_conf["method"] == "gaussian":
        wave += gaussian(
            wave_conf["x0"], wave_conf["y0"], wave_conf["w"], wave_conf["kx"], wave_conf["ky"],
            min_x, max_x, nbp_x,
            min_y, max_y, nbp_y
        ) * wave_conf["weight"]
    elif wave_conf["method"] == "2dho":
        wave += ho2d(
            wave_conf["nx"], wave_conf["ny"],
            min_x, max_x, nbp_x,
            min_y, max_y, nbp_y
        ) * wave_conf["weight"]
    else:
        logging.error("no wave function definition provided")
        exit(1)
wave = normalize(wave)

# DB add
bin_field = bson.binary.Binary(pickle.dumps(np.array(np.real(field), dtype=float, order="F")))
bin_wave_r = bson.binary.Binary(pickle.dumps(np.array(np.real(wave), dtype=float, order="F")))
bin_wave_i = bson.binary.Binary(pickle.dumps(np.array(np.imag(wave), dtype=float, order="F")))

## add wave
wave = {
    "run_id": run_id,
    "niter": niter,
    "iter": 0,
    "field": bin_field,
    "r": bin_wave_r,
    "i": bin_wave_i,
    "solver": conf_solver,
    "dim": conf_dim
}

try:
    wave_coll.insert_one(wave)
    logging.info(f"inserted wave0")
except pymongo.errors.OperationFailure as e:
    logging.error(f"ERROR: {e}")
    exit(1)
