def test_db():
    from pymongo import MongoClient
    import pymongo.errors

    username, password, host, dbname = 'user1', 'pwd1', '127.0.0.1', 'psa_projet'
    client = MongoClient(f'mongodb://{username}:{password}@{host}/{dbname}')
    try:
        print(client.list_database_names())  # list databases
        db = client[dbname]
        print(db.list_collection_names())  # list collections
    except pymongo.errors.OperationFailure as e:
        print(f'ERROR: {e}')
        assert False

