<!DOCTYPE html>
<html>
  <head>
    <title>PSA</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="core/fonts/mono.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/animate.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/cinescript.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_core.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/mermaid.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/gitgraph.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_ensiie.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/katex.css"> 
  </head>
  <body>
    <textarea id="source" readonly>

class: middle

.title[PSA Project]

.footnote[
Boyer Léa, Charrier Paul, Le Piouff Junah - ENSIIE - 2022 
]



---

# Présentation du projet


* Créer un solveur 2D-FD pour l'équation de Schrödinger, cette fois ci non relative et dépendante du temps.


* Utiliser ce solver pour produire une animation du résultat avec Paraview.


* Gérer le monitoring d'un run et son restart.


* Utiliser python pour gérer MongoDB, et donc développer des bindings python/c++.


* Implémenter différents cas d'utilisations (diffraction, tunnel..).


.hcenter.w40[
<video controls="yes" loop="yes" preload="auto" width="100%" height="auto" data-setup="{}" loop>
<source src="propag_2D.webm" type="video/webm" />
</video>
]

---

# Objectif du projet

:white_check_mark: Generateur du potentiel et des fonctions d'ondes (gaussian)

:white_check_mark: Solveur (c++)

:white_check_mark: Bindings python du solveur

:white_check_mark: Lancement d'un run (avec restart)

:arrow_right: Tests

:arrow_right: Post processing (Paraview)

:arrow_right: Fonctions d'ondes (2dho, 2dho-mix)

:arrow_right: GridFS

---

# Objectif final

## Généralités

Nous souhaitons simuler puis observer l'évolution d'une particule dans le temps et dans l'espace.  

L'évolution dans le temps et l'espace d'une particule, associée à une onde, est décrite par l'équation de Schrödinger: 
$ i\hbar\frac{\partial}{\partial t}\psi(x,y,t) = \hat{\mathcal{H}}\_{(x,y)}\psi(x,y,t) $

with the 2D-Hamiltonian operator defined as:

$$ 
\hat{\mathcal{H}}\_{(x,y)}\equiv \frac{\hat{p}\_{(x)}^2}{2m} + \frac{\hat{p}\_{(y)}^2}{2m} + \hat{V}(x,y)
$$


and the momentum operators defined as
$ \forall u \in \{x,y\},\hspace{5mm}\hat{p}\_{(u)}\equiv -i\hbar\frac{\partial}{\partial u}. $

---

# Dérivations des schémas numériques

## Objectif

L'équation précédente est une équation décrivant un espace et un temps continu.  
On souhaite discrétiser le temps et l'espace afin de pouvoir permettre le calcul sur des matrices et s'affranchir des dérivées.  
On maintient des matrices de taille finie, car on estime que le potentiel est infini en dehors du domaine.  
La discrétisation se base sur des méthodes de différences finies, et donc sur des approximations par développement de Taylor.  

On rappelle la formule de Taylor à l'ordre 1:  
Si $f$ est une fonction dérivable en $x \in \mathbb{R}$ et $h \in \mathbb{R}$ , alors: 

$$ 
f(x+h) = f(x) + f'(x) \cdot h + R\_1(x+h) 
$$

D'où l'approximation:  

$$ 
f'(x) \approx \frac{f(x+h) - f(x)}{h} 
$$

---

# Dérivations des schémas numériques

## FTCS - preuve

$$
i\hbar\frac{\partial}{\partial t}\psi(x,y,t) = \hat{\mathcal{H}}\_{(x,y)}\psi(x,y,t) \\\\
\frac{-\hbar\frac{\partial^2\psi}{\partial x^2}}{2m} - \frac{\hbar\frac{\partial^2\psi}{\partial y^2}}{2m} + V(x,y) \\\\
$$

D'où l'approximation:  

$$
\begin{aligned}
i\hbar\frac{\psi(t+dt)-\psi(t)}{dt} &= -\frac{\hbar^2}{2m} \cdot \frac{\partial}{\partial x} \left( \frac{\psi(x+dx) - \psi(x-dx)}{2dx} \right) \\\\ 
&- \frac{\hbar^2}{2m} \cdot \frac{\partial}{\partial y} \left( \frac{\psi(y+dy) - \psi(y-dy)}{2dy} \right) \\\\
&+ V 
\end{aligned}
$$

$$
\begin{aligned}
i\hbar\frac{\psi(t+dt)-\psi(t)}{dt} &\approx  -\frac{\hbar^2}{2m} \cdot  \frac{\psi(x+2dx) - 2\psi(x) + \psi(x+2dx)}{4dx^2} \\\\
&- \frac{\hbar^2}{2m} \cdot \frac{\psi(y+2dy) - 2\psi(y) + \psi(y+2dy)}{4dy^2} \\\\
&+ V 
\end{aligned}
$$

---
# Dérivations des schémas numériques

$$
\begin{aligned}
\psi(t+dt) &= \frac{-dt \hbar}{2mi} ( \frac{\psi(x+2dx) - 2\psi(x) + \psi(x+2dx)}{4dx^2} \\\\
&+  \frac{\psi(y+2dy) - 2\psi(y) + \psi(y+2dy)}{4dy^2} ) \\\\
&+ \frac{dt V}{i \hbar} + \psi{t} 
\end{aligned}
$$

---

En changeant la variable $dx$ par $\frac{dx}{2}$, la formule devient:

$$
\begin{aligned}
\psi(t+dt) &= \frac{i dt \hbar}{2m} (\frac{\psi(x+dx) - 2\psi(x) + \psi(x+dx)}{dx^2} \\\\
&+  \frac{\psi(y+dy) - 2\psi(y) + \psi(y+dy)}{dy^2} ) \\\\
&+ \frac{dt V}{i \hbar} + \psi{t}
\end{aligned}
$$

Qui est équivalente à:

$$
\begin{aligned}
\psi^{(t+dt)} = \psi + i.dt\Bigg[ & (-\frac{1}{\hbar}V(x,y) - \frac{\hbar}{m.dx^2}-\frac{\hbar}{m.dy^2})\psi \\\\
&+ i\frac{\hbar.dt}{2m.dx^2}(\psi\_{x+dx}+\psi\_{x-dx}) \\\\
&+ i\frac{\hbar.dt}{2m.dy^2}(\psi\_{y+dy}+\psi\_{y-dy})\Bigg]
\end{aligned}
$$

---

# Dérivation des schémas numériques

## FTCS - code

```cpp
#define NO_SHIFT psi.submat(1, 1, nx, ny)
#define XPDX psi.submat(2, 1, nx+1, ny)
#define XMDX psi.submat(0, 1, nx-1, ny)
#define YPDY psi.submat(1, 2, nx, ny+1)
#define YMDY psi.submat(1, 0, nx, ny-1)

arma::cx_mat Solver::FTCS(arma::cx_mat psi) {
    int nx = psi.n_rows;
    int ny = psi.n_cols;
    psi.insert_cols(0, arma::zeros<arma::cx_colvec>(nx));
    psi.insert_cols(ny+1, arma::zeros<arma::cx_colvec>(nx));
    psi.insert_rows(0, arma::zeros<arma::cx_rowvec>(ny+2));
    psi.insert_rows(nx+1, arma::zeros<arma::cx_rowvec>(ny+2));

    arma::mat cst = -v/hbar -(hbar/(m*dx*dx)) - (hbar/m*dy*dy);
    arma::cx_mat x = (i*hbar*dt/(2*m*dx*dx))*(XPDX + XMDX);
    arma::cx_mat y = (i*hbar*dt/(2*m*dy*dy))*(YPDY + YMDY);

    return NO_SHIFT + i*dt*(cst*psi + x + y);
}

```
---

# Dérivations des schémas numériques

## BTCS - preuve
`$$
\begin{aligned}
i\hbar\frac{\partial}{\partial t}\psi(x,y,t) &= \hat{\mathcal{H}}_{(x,y)}\psi(x,y,t))\\
&= \frac{-\hbar\frac{\partial^2\psi}{\partial x^2}}{2m} - \frac{\hbar\frac{\partial^2\psi}{\partial y^2}}{2m} + V(x,y)\\
\end{aligned}
$$`
D'où l'approximation:
`$$
\begin{aligned}
i \hbar \frac{\psi(t-dt) - \psi(t)}{dt} &= \frac{-\hbar^2}{2m} \left(\frac{\psi(x+dx) - 2\psi(x) + \psi(x-dx)}{dx^2}\right)\\
&+\frac{-\hbar^2}{2m} \left( \frac{\psi(y+dy) - 2\psi(y) + \psi(y-dy)}{dy^2} \right)\\
&+ V(x,y) \cdot \psi(t+dt)\\
\end{aligned}
$$`

`$$
\begin{aligned}
\psi(t-dt) - \psi(t) &= \frac{-dt\hbar}{i\cdot 2m} \left( \frac{\psi(x+dx,t+dt)+\psi(x-dx,t+dt)}{dx^2}\right)\\
&+\frac{-dt\hbar}{i\cdot 2m} \left(\frac{\psi(y+dy,t+dt)+\psi(y-dy,t+dt)}{dy^2} \right)\\
&\frac{-dt\hbar}{i\cdot 2m} \left(\frac{-2\psi(t+dt)}{dx^2}- \frac{2\psi(t+dt)}{dy^2} \right)\\
&+ \frac{dt}{i \hbar} \cdot V \cdot \psi(t+dt)
\end{aligned}
$$`
 
---

# Dérivation des schémas numériques

D'où le résultat:
`$$
\begin{aligned}
\psi(t+dt)&\left(1 - \frac{dt \cdot \hbar}{dx^2 \cdot i m} - \frac{dt \cdot \hbar}{dy^2 \cdot i m} + \frac{dt}{i\hbar}V \right)\\
&= \frac{i dt \hbar}{2m} \left( \frac{\psi(x+dx,t+dt) + \psi(x-dx)}{dx^2} \right)\\
&+ \frac{i dt \hbar}{2m} \left( \frac{\psi(y+dy,t+dt) + \psi(y-dy,t+dt)}{dy^2} \right)\\
&+\psi(t)
\end{aligned}
$$`
Ce qui équivalent à:
`$$
\begin{aligned}
\psi^{(t+dt)} = \psi + i\frac{dt}{2}\Bigg[ & \left( -\frac{1}{\hbar}V(x,y) - \frac{\hbar}{m.dx^2} - \frac{\hbar}{m.dy^2}\right)\psi\\
&+ \frac{\hbar}{2m.dx^2}\left(\psi_{x+dx}+\psi_{x-dx}\right) + \frac{\hbar}{2m.dy^2}\left(\psi_{y+dy}+\psi_{y-dy}\right)\\
&+ \left( -\frac{1}{\hbar}V(x,y) - \frac{\hbar}{m.dx^2} -\frac{\hbar}{m.dy^2}\right)\psi^{(t+dt)}\\
&+ \frac{\hbar}{2m.dx^2}\left(\psi^{(t+dt)}_{x+dx}+\psi^{(t+dt)}_{x-dx}\right)\\
&+ \frac{\hbar}{2m.dy^2}\left(\psi^{(t+dt)}_{y+dy}+\psi^{(t+dt)}_{y-dy}\right)\Bigg]
\end{aligned}
$$`

---

# Dérivation des schémas numériques

## BTCS - résolution du schéma implicite

Nous obtenons un résultat implicite. Une étape nécessaire est donc nécessaire pour retrouver la solution.  
Nous contruisons une suite qui converge vers le résultat voulu:

`$$g^{(0)}(x) \equiv f^{(t)}(x)$$`
`$$g^{(n+1)}(x) = F\left(g^{(n)}(x+dx),g^{(n)}(x-dx),f^{(t)}(x+dx),f^{(t)}(x),f^{(t)}(x-dx)\right)$$`

---

# Dérivation des schémas numériques

## BTCS - code

```cpp
#define B_NO_SHIFT psi_b->submat(1, 1, nx, ny)
#define A_NO_SHIFT psi_a->submat(1, 1, nx, ny)
#define A_XPDX psi_a->submat(2, 1, nx+1, ny)
#define A_XMDX psi_a->submat(0, 1, nx-1, ny)
#define A_YPDY psi_a->submat(1, 2, nx, ny+1)
#define A_YMDY psi_a->submat(1, 0, nx, ny-1)

arma::cx_mat Solver::BTCS(arma::cx_mat psi) {
    /**
     * use numpy array conventions for x and y axes
     */
    int nx = psi.n_rows;
    int ny = psi.n_cols;

    arma::mat A(nx, ny, arma::fill::zeros);
    arma::mat B(nx, ny, arma::fill::zeros);
    arma::cx_mat psi_tpdt = arma::cx_mat(A,B);

    arma::cx_mat* psi_a = &psi;
    arma::cx_mat* psi_b = &psi_tpdt;
    arma::cx_mat* swp;

    padding(psi_a);
    padding(psi_b);

    while(1) {
        // *INDENT-OFF*
        B_NO_SHIFT = A_NO_SHIFT + i*dt*(
            (-1/hbar*v-hbar/(m*dx*dx)-hbar*(m*dy*dy)) % A_NO_SHIFT
            +
            hbar/(2*m*dx*dx)*(A_XPDX + A_XMDX)
            +
            hbar/(2*m*dy*dy)*(A_YPDY + A_YMDY)
        );
        // *INDENT-ON*
        if (arma::norm(psi_tpdt - A_NO_SHIFT) < eps) {  //use __builtin_expect((x),0)
            break;
        } else {
            // swap psi_a and psi_b
            swp = psi_a;
            psi_a = psi_b;
            psi_b = swp;
        }
    }
    return psi_tpdt;
}
```

---

# Dérivation des schémas numériques

## CTCS

Le CTCS est défini comme étant la somme du FTCS et du BTCS.  
La démonstration du résultat consiste à sommer les deux précédents et réarranger les termes.  
d'où le résultat:  
`$$
\begin{aligned}
\psi^{(t+dt)} = \psi + i\frac{dt}{2}\Bigg[ & \left( -\frac{1}{\hbar}V(x,y) - \frac{\hbar}{m.dx^2} - \frac{\hbar}{m.dy^2}\right)\psi\\
&+ \frac{\hbar}{2m.dx^2}\left(\psi_{x+dx}+\psi_{x-dx}\right)\\
&+ \frac{\hbar}{2m.dy^2}\left(\psi_{y+dy}+\psi_{y-dy}\right)\\
&+ \left( -\frac{1}{\hbar}V(x,y) - \frac{\hbar}{m.dx^2} -\frac{\hbar}{m.dy^2}\right)\psi^{(t+dt)}\\
&+ \frac{\hbar}{2m.dx^2}\left(\psi^{(t+dt)}_{x+dx}+\psi^{(t+dt)}_{x-dx}\right)\\
&+ \frac{\hbar}{2m.dy^2}\left(\psi^{(t+dt)}_{y+dy}+\psi^{(t+dt)}_{y-dy}\right)\Bigg]
\end{aligned}
$$`

---

# Dérivation des schémas numériques

## CTCS - code

```cpp
arma::cx_mat Solver::CTCS(arma::cx_mat psi_t) {
    /**
     * use numpy array conventions for x and y axes
     */
    int nx = psi_t.n_rows;
    int ny = psi_t.n_cols;

    arma::mat A(nx, ny, arma::fill::zeros);
    arma::mat B(nx, ny, arma::fill::zeros);
    arma::cx_mat psi_tpdt = arma::cx_mat(A,B);

    arma::cx_mat psi = psi_t; //original value of psi is necessary
    arma::cx_mat* psi_a = &psi_t;
    arma::cx_mat* psi_b = &psi_tpdt;
    arma::cx_mat* swp;

    padding(psi_a);
    padding(psi_b);

    while(true) {
        // *INDENT-OFF*
        B_NO_SHIFT = NO_SHIFT + i*dt*0.5*(
            (-1/hbar*v-hbar/(m*dx*dx)-hbar*(m*dy*dy)) % NO_SHIFT
            +
            hbar/(2*m*dx*dx)*(XPDX + XMDX)
            +
            hbar/(2*m*dy*dy)*(YPDY + YMDY)
            +
            (-1/hbar*v-hbar/(m*dx*dx)-hbar*(m*dy*dy)) % A_NO_SHIFT
            +
            hbar/(2*m*dx*dx)*(A_XPDX + A_XMDX)
            +
            hbar/(2*m*dy*dy)*(A_YPDY + A_YMDY)
        );
        // *INDENT-ON*        
        if (arma::norm(psi_tpdt - A_NO_SHIFT) < eps) {  //use __builtin_expect((x),0)
            break;
        } else {
            // swap psi_a and psi_b
            swp = psi_a;
            psi_a = psi_b;
            psi_b = swp;
        }
    }
    return psi_tpdt;
}
```

---
# Pré-requis

## Installation

* Armadillo
* Python 3:
    * numpy: matrices
    * PIL: convertir une image en matrice
    * bson et pickle pour convertir les matrices numpy en objet MongoDB
    * pymongo pour gérer la base de données  
    * logging et argparse

## Développement

* cxxtest 

Pour générer les bindings, inclure:
* armanpy headers
* numpy headers
* boost headers


---

# Structure du projet

* Hierarchie simplifiée du projet

.alert.tree[
* bindings/
* doc/
* src/
    * release/
        * libsolver.so  # solveur (c++)
    * solver.cpp
    * solver.h
    * Makefile
* generator.py
* post.py               
* solver.py             # module solveur (bindings)
* _solver.cpython-310-x86_64-linux-gnu.so # (bindings)
* run.py    # solveur (python)
* Makefile
]

---

# Structure du projet

.mermaid.shadow.w60.hcenter[
    graph TD
    A("MongoDB") 
    B("VTK file(s)")
    C("generator.py")
    D("run.py")
    E("post.py")
    F("solver (c++)")
    C -- write --> A 
    D -- read/write --> A
    E -- read --> A
    E -- write --> B
    F -- bindings --> D
    style A fill:#090 
    style B fill:#B00 
    style F fill:#A09
]

* Les différents programmes

    1. Le Generateur
    2. Le Solver
    3. Le Post-Processeur

* S'échangent les informations grâce à MongoDB

---

# `generator`

* Prend en argument un fichier JSON:

```json
{
    "field": "examples/young.jpg",
    "dim": {
        "min_x": -1,
        "max_x": 1,
        "nbp_x": 1e2,

        "min_y": -1,
        "max_y": 1,
        "nbp_y": 1e2,

        "dt": 1e-3,
        "iter": 1e2
    },
    "wave": {
        "gaussian": {
            "x0": 2,
            "y0": 1,
            "w": 4,
            "kx":1,
            "ky":0,
            "A": 1
        }
    }
}
```

* Pour l'instant, la seule fonction d'onde implémentée est une gaussienne.
* Le potentiel que l'on utilise pour nos tests représente des fentes de Young.

---

# `generator`

A la fin de la génération sont ajoutés à la base de données:
* le potentiel
* les paramètres du domaine
* le nombre d'itérations a exécuter

```json
{
    _id: ObjectID(''),
    field: Binary(''),
    min_x: -1,
    max_x: 1,
    nvp_x: 100,
    min_y: -1,
    max_y: 1,
    nvp_y: 100,
    dt: 0.001,
    iter: 100,
}
```

* la fonction d'onde initiale 

```json
{
    'run_id': ObjectID('')
    'iter': 0
    'r': Binary(''),
    'i': Binary('')
}
```

En cas de panne, l'utilisateur peut reprendre un `run`.

---
# `generator`

## Modélisation des fentes de Young

.hcenter.shadow.w100.animated.fadeInUp.wait1s[
<center> <iframe src="../examples/young.jpg" width="auto" height="auto" float=left></iframe> </center>
]

L'image est présente dans le json.

:arrow_right: Il faut la convertir en matrice : on utilise la classe Image de la librairie PIL.

* On peut aussi modéliser un effet tunnel comme potentiel.

.hcenter.shadow.w100.animated.fadeInUp.wait1s[
<center> <iframe src="../examples/tunnel.jpg" width="auto" height="auto" float=left></iframe> </center>
]
---

# `solver`

## Solver class

```c
class Solver {
public :
    Solver(double dx, double dy, double dt, double eps, arma::mat v, std::string method);
    arma::cx_mat solve(arma::cx_mat psi);
private:
    double dx;
    double dy;
    double dt;
    double eps;

    arma::mat v;
    std::string method;

    arma::cx_mat FTCS(arma::cx_mat psi);
    arma::cx_mat BTCS(arma::cx_mat psi);
    arma::cx_mat CTCS(arma::cx_mat psi);

    static void padding(arma::cx_mat* psi);
};
```
* Constructeur:
    * dx, dy, dt, v
    * eps: précision de la convergence de BTCS et CTCS 
    * method: la méthode utilisée (FTCS, BTCS, CTCS)
    * utilisation de std::move dans le constructeur pour éviter les copies
* `solve()`:
    * switch en fonction de `method` 
    * entrée: la fonction d'onde au temps t
    * sortie: la fonction d'onde au temps t+dt


---

# `solver`

## bindings

```py
from setuptools import setup, Extension

module = Extension('_solver',
                    include_dirs = ['armanpy'],
                    libraries = ['m', 'z', 'armadillo', 'solver'],
                    library_dirs= ['../src/release/'],
                    sources = ['solver.i'],
                    swig_opts = ["-c++", "-Wall", "-I.", "-Iarmanpy"])

setup (name = 'package_solver',
       py_modules = ['solver'],
       version = '1.0',
       description = 'The solver',
       ext_modules = [module])

```

* arma::cx_mat <=> numpy complex array
* `libsolver.so`
* création de `solver.py` et `_solver.cpython-310-x86_64-linux-gnu.so`
* fourni au client (à la racine du projet)

---

# `solver`

## proc maps

```
7fb760047000-7fb76005a000 r--p 00000000 08:15 7221775                    /home/jun/cours/psa-projet/src/release/libsolver.so
7fb76005a000-7fb760087000 r-xp 00013000 08:15 7221775                    /home/jun/cours/psa-projet/src/release/libsolver.so
7fb760087000-7fb76008c000 r--p 00040000 08:15 7221775                    /home/jun/cours/psa-projet/src/release/libsolver.so
7fb76008c000-7fb76008d000 r--p 00044000 08:15 7221775                    /home/jun/cours/psa-projet/src/release/libsolver.so
7fb76008d000-7fb76008e000 rw-p 00045000 08:15 7221775                    /home/jun/cours/psa-projet/src/release/libsolver.so
...
7fb78003a000-7fb78004c000 r--p 00000000 08:15 3149407                    /home/jun/cours/psa-projet/_solver.cpython-310-x86_64-linux-gnu.so
7fb78004c000-7fb780070000 r-xp 00012000 08:15 3149407                    /home/jun/cours/psa-projet/_solver.cpython-310-x86_64-linux-gnu.so
7fb780070000-7fb780083000 r--p 00036000 08:15 3149407                    /home/jun/cours/psa-projet/_solver.cpython-310-x86_64-linux-gnu.so
7fb780083000-7fb780085000 r--p 00048000 08:15 3149407                    /home/jun/cours/psa-projet/_solver.cpython-310-x86_64-linux-gnu.so
7fb780085000-7fb78008e000 rw-p 0004a000 08:15 3149407                    /home/jun/cours/psa-projet/_solver.cpython-310-x86_64-linux-gnu.so
```

---

# `solver`

## run.py

```py
a = solver.Solver(dx=dx, dy=dy, dt=dt, eps=eps, v=v, method="")
max_iter = int(data["iter"])
current_iter = int(wave["iter"])

if current_iter != 0:
    logger.warning(f"resuming previous run from iter {current_iter}/{max_iter}")

while current_iter < max_iter:
    logger.info(f"iter: {current_iter}/{max_iter}")
    wave = a.solve(wave)
    current_iter += 1

    # insert partial result in db
    bin_wave_r = bson.binary.Binary(pickle.dumps(np.array(np.real(wave), dtype=float, order="F")))
    bin_wave_i = bson.binary.Binary(pickle.dumps(np.array(np.imag(wave), dtype=float, order="F")))
    try:
        wave_data = {
            "run_id": run_id
            "iter": current_iter,
            "r": bin_wave_r,
            "i": bin_wave_i
        }
        wave_id = runs.insert_one(wave_data)
    except pymongo.errors.OperationFailure as e:
        print(f"ERROR: {e}")

```

* Mécanisme de redémarage

:warning: Attention a bien fournir une matrice numpy en ordre Fortran

---

# `post`

* Prend en argument un numéro de run.


* Récupère toutes les matrices générées par le solver à toutes les steps dans MongoDB pour ce run.


:arrow_right: Reste à implémenter : Convertir chaque matrice (step + réel/imaginaire) en objet VTK.
---

# GridFS

* Limite de 16M par documents sur Mongodb (atteinte pour une matrice 1000x1000)
* Nom de fichier unique : `{run_id}_{step}_wave_r`


    </textarea>

    <script src="core/javascript/remark.js"></script>
    <script src="core/javascript/katex.min.js"></script>
    <script src="core/javascript/auto-render.min.js"></script>
    <script src="core/javascript/emojify.js"></script>
    <script src="core/javascript/mermaid.js"></script>
    <script src="core/javascript/term.js"></script>
    <script src="core/javascript/jquery-2.1.1.min.js"></script>
    <script src="core/javascript/extend-jquery.js"></script>
    <script src="core/javascript/cinescript.js"></script>
    <script src="core/javascript/gitgraph.js"></script>
    <script>

    // === Remark.js initialization ===
    var slideshow = remark.create({
      highlightStyle: 'monokai',
      countIncrementalSlides: false,
      highlightLines: false
    });

    // === Mermaid.js initialization ===
    mermaid.initialize({
      startOnLoad: false,
      cloneCssStyles: false,
      flowchart:{
        height: 50
      },
      sequenceDiagram:{
        width: 110,
        height: 30
      }
    });

    function initMermaid(s) {
      var diagrams = document.querySelectorAll('.mermaid');
      var i;
      for(i=0;i<diagrams.length;i++){
        if(diagrams[i].offsetWidth>0){
          mermaid.init(undefined, diagrams[i]);
        }
      }
    }

    slideshow.on('afterShowSlide', initMermaid);
    initMermaid(slideshow.getSlides()[slideshow.getCurrentSlideIndex()]);

    
    // === Emojify.js initialization ===
    emojify.run();

    // === Cinescript initialization ===
    $(document).ready(init_cinescripts);

    renderMathInElement(document.body,{delimiters: [{left: "$$", right: "$$", display: true}, {left: "$", right: "$", display: false}], ignoredTags: ["script", "noscript", "style", "textarea", "pre"] });

    </script>
  </body>
</html>

