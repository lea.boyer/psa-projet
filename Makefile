all: user

# User
user: install vtk

install:
	$(MAKE) -C bindings

doc:
	$(MAKE) -C doc

# Dev
dev: doc bindings install vtk

src:
	$(MAKE) -C src CXXFLAGS_BUILD='-O3 -march=native -fopenmp'

debug:
	$(MAKE) -C src CXXFLAGS_BUILD='-g' debug

bindings: src
	$(MAKE) -C bindings build

clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean
	$(MAKE) -C bindings clean

format:
	$(MAKE) -C src format

vtk:
	mkdir -p vtk

.PHONY: all install doc src debug clean format
