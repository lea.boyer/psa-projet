"""!
@brief All the computational functions used in run.py or generator.py.
"""
import logging
from math import sqrt, factorial, exp

import numpy
import numpy as np
from PIL import Image
from pymongo import MongoClient
from scipy import special

from const import pi, hbar, m, w

username, password, host, dbname = 'user1', 'pwd1', '127.0.0.1', 'psa_projet'
client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
db = client.psa_projet
wave_coll = db["wave_coll"]


def norm(wave):
    """!
    Calculated the norm of a wave
    @return norm
    """
    return np.sqrt(np.sum(np.abs(wave) ** 2))


def normalize(wave):
    """!
    Normalizes a wave
    @return wave normalized
    """
    return wave / norm(wave)


def load_field(filename, nbp_x, nbp_y):
    """!
    Load a field from an image
    @return field converted in a np.array
    """
    im = Image.open(filename).convert('L')
    if im.size != (nbp_x, nbp_y):
        logging.error(f"image does not have expected size | expected {nbp_x}x{nbp_y}")
        exit(1)
        im = im.resize((nbp_x, nbp_y), )
    return np.array(im, dtype=float)


def gaussian(x0, y0, w, kx, ky, min_x, max_x, nbp_x, min_y, max_y, nbp_y):
    """!
    Calculates a gaussian wave from parameters
    @return normalized wave
    """
    xlin = np.linspace(int(min_x), int(max_x), int(nbp_x))
    ylin = np.linspace(int(min_y), int(max_y), int(nbp_y))
    wave = np.zeros((nbp_x, nbp_y), dtype=complex)

    for i, x in enumerate(xlin):
        for j, y in enumerate(ylin):
            wave[i, j] = np.exp(-(np.power(x - x0, 2) + np.power(y - y0, 2)) / np.power(w, 2)) * np.exp(
                1j * (kx * x + ky * y))
    return normalize(wave)


def ho1d(n, u):
    """!
    Calculates the 1-dimension Harmonic Oscillator from parameters
    @return 1-dimension Harmonic Oscillator result
    """
    hnu = special.hermite(n)
    return (
        1/(sqrt(2**n*factorial(n)))
        * ((m*w)/(pi*hbar))**0.25
        * np.exp(-(m*w*u**2)/2*hbar)
        * hnu(np.sqrt((m*w)/hbar)*u)
    )


def ho2d(nx, ny, min_x, max_x, nbp_x, min_y, max_y, nbp_y):
    """!
    Calculates the 2-dimension Harmonic Oscillator from parameters and 1-dimension one
    @return 2-dimension Harmonic Oscillator result (normalized wave)
    """
    xlin = np.linspace(min_x, max_x, nbp_x)
    ylin = np.linspace(min_y, max_y, nbp_y)
    psix = ho1d(nx, xlin)
    psiy = ho1d(ny, ylin)
    wave = np.outer(psix, psiy)
    return normalize(wave)


def hov(min_x, max_x, nbp_x, min_y, max_y, nbp_y):
    xlin = np.linspace(min_x, max_x, nbp_x)
    ylin = np.linspace(min_y, max_y, nbp_y)
    field = np.zeros((nbp_x, nbp_y))
    for i, x in enumerate(xlin):
        for j, y in enumerate(ylin):
            field[i, j] = 0.5*m*w**2*(x**2+y**2)
    return field

