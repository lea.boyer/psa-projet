import numpy as np
import solver
from helper import load_field, gaussian, norm

v = np.array(load_field("examples/empty251.jpg", 251, 251), dtype=float, order="F")
wave = gaussian(
        0, 0, 1, 0, 5,
        -10, 10, 251,
        -10, 10, 251
)
wave = np.array(wave, dtype=complex, order="F")

dx = 20/251
dy = 20/251

def test_ftcs():
    dt = 0.000025
    eps = 0

    a = solver.Solver(psi=wave, v=v, dx=dx, dy=dy, dt=dt, eps=eps)
    for _ in range(100):
        a.FTCS()
    assert (1.0-norm(a.psi) < 0.01)

def test_btcs():
    dt = 0.0005
    eps = 1e-5

    a = solver.Solver(psi=wave, v=v, dx=dx, dy=dy, dt=dt, eps=eps)
    for _ in range(100):
        a.BTCS()
    assert (1.0-norm(a.psi) < 0.01)

def test_ctcs():
    dt = 0.002
    eps = 1e-5

    a = solver.Solver(psi=wave, v=v, dx=dx, dy=dy, dt=dt, eps=eps)
    for _ in range(100):
        a.CTCS()
    assert (1.0-norm(a.psi) < 0.01)

